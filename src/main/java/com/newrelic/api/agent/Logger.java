package com.newrelic.api.agent;

import java.text.MessageFormat;
import java.util.logging.Level;

/**
 * The agent's logging interface.
 * 
 * @author sdaubin
 * 
 */
public interface Logger {

    /**
     * Returns true if the given log level will be logged. Generally this method should NOT be used - just call the
     * {@link #log(Level, String, Object...)} methods with the message broken into parts. The overhead of the
     * concatenation will not be incurred if the log level isn't met.
     * 
     * @param level
     * @return
     */
    boolean isLoggable(Level level);

    /**
     * Concatenate the given strings and log them at the given level. If a part is null, its value will be represented
     * as "null". If a part is a Class, the value of {@link Class#getName()} will be used.
     * 
     * @param level
     * @param pattern A message format pattern in the {@link MessageFormat} style.
     * @param parts
     */
    void log(Level level, String pattern, Object... parts);

    /**
     * Log a message with associated Throwable information.
     * 
     * @param pattern A message format pattern in the {@link MessageFormat} style.
     * 
     */
    void log(Level level, Throwable t, String pattern, Object... msg);

    /**
     * Concatenate the given strings and log them at the given level. If a part is null, its value will be represented
     * as "null". If a part is a Class, the value of {@link Class#getName()} will be used.
     * 
     * @param level
     * @param pattern A message format pattern in the {@link MessageFormat} style.
     * @param parts
     */
    void logToChild(String childName, Level level, String pattern, Object... parts);
}
