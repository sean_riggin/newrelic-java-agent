package com.newrelic.api.agent;

/**
 * The type-specific headers collection of an inbound message
 */
public interface InboundHeaders {
    /**
     * Return the type of these headers
     * 
     * @return the type of these headers
     */
    HeaderType getHeaderType();

    /**
     * Returns the value of the specified request header as a String.
     * 
     * @param name The name of the desired request header.
     * @return Value of the input specified request header.
     */
    String getHeader(String name);
}
