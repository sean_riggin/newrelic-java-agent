package com.newrelic.api.agent;

/**
 * A transaction represents a unit of work in an application. It may be a single web request or a scheduled background
 * task. To indicate that a method is the starting point of a transaction, mark it with a {@link Trace} annotation and
 * set {@link Trace#dispatcher()} to true.
 * 
 * @author sdaubin
 * 
 * @see Agent#getTransaction()
 * @see Trace#dispatcher()
 */
public interface Transaction {

    /**
     * Sets the current transaction's name using the given priority. Higher priority levels are given precedence, and if
     * the name is set many times with the same priority, the first call wins unless override is true.
     * 
     * @param namePriority The priority of the new transaction name.
     * @param override Overrides the current transaction name if it has the same priority level (or lower).
     * @param category The type of transaction. This is the second segment of the full transaction metric name.
     * @param parts The category and all of the parts are concatenated together with / characters to create the full
     *        name
     * @return Returns true if the transaction name was successfully changed
     */
    boolean setTransactionName(TransactionNamePriority namePriority, boolean override, String category, String... parts);

    /**
     * Returns true if the transaction name has been set. This method is inherently unreliable in the presence of
     * transactions with multiple threads, because another thread may set the transaction name after this method returns
     * but before the caller can act on the return value.
     */
    boolean isTransactionNameSet();

    /**
     * Returns this transaction's last tracer.
     * 
     * @deprecated use {@link getTracedMethod}.
     */
    @Deprecated
    TracedMethod getLastTracer();

    /**
     * Returns the TracedMethod enclosing the caller.
     * 
     * @return the TracedMethod enclosing the caller. The return value is null if the caller is not within a
     *         transaction.
     */
    TracedMethod getTracedMethod();

    /**
     * Ignore this transaction so that none of its data is reported to the New Relic service.
     */
    void ignore();

    /**
     * Ignore the current transaction for calculating Apdex score.
     */
    void ignoreApdex();
}
