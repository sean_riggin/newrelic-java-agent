package com.newrelic.api.agent;

/**
 * The type-specific headers collection of an outbound message
 */
public interface OutboundHeaders {
    /**
     * Return the type of these headers
     * 
     * @return the type of these headers
     */
    HeaderType getHeaderType();

    /**
     * Sets a response header with the given name and value.
     * 
     * @param name Name to add to the response header.
     * @param value Value to add to the response header.
     */
    void setHeader(String name, String value);
}
