package com.newrelic.api.agent;


/**
 * The New Relic Java agent's api.
 * 
 * @author sdaubin
 * 
 */
public interface Agent {

    /**
     * Returns the current traced method. This can only be invoked within methods that are traced.
     * 
     * @return
     * @see Trace
     */
    TracedMethod getTracedMethod();

    /**
     * Returns the current transaction, creating one if it doesn't currently exist.
     * 
     * @return
     */
    Transaction getTransaction();

    /**
     * Returns a logger that logs to the New Relic java agent log file.
     * 
     * @return
     */
    Logger getLogger();

    /**
     * Returns the agent's configuration.
     * 
     * @return
     */
    Config getConfig();

    /**
     * Returns a metric aggregator that can be used to record metrics that can be viewed through custom dashboards.
     * 
     * @return
     */
    MetricAggregator getMetricAggregator();
}
