package com.newrelic.api.agent;

/**
 * Provides access to agent configuration settings. The key names are flattened representations of values in the
 * configuration file.
 * 
 * Example keys: <br/>
 * transaction_tracer.enabled<br />
 * instrumentation.hibernate.stat_sampler.enabled
 * 
 * @author sdaubin
 * 
 */
public interface Config {

    /**
     * Get a setting value.
     * 
     * @return the property value or null if absent
     */
    <T> T getValue(String key);

    /**
     * Get a setting value.
     * 
     * @return the property value of the default value if absent
     * 
     */
    <T> T getValue(String key, T defaultVal);
}
