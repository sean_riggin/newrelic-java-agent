package com.newrelic.api.agent;

import java.util.concurrent.TimeUnit;

/**
 * This aggregator allows metrics to be reported which can be viewed through custom dashboards.
 * 
 * @author sdaubin
 * 
 */
public interface MetricAggregator {

    /**
     * Records an unscoped metric. This metric will not be shown in the breakdown of time for a transaction, but it can
     * be displayed in a custom dashboard.
     * 
     * @param name The metric name
     * @param totalTime The total time value. This is the value that custom dashboards will display (often the average
     *        of the value)
     * @param exclusiveTime The exclusive time for this metric. This value is primarily used by the agent for
     *        transaction blame metrics, but it is exposed here for future uses.
     * @param timeUnit The time unit of the values passed into this method
     */
    void recordResponseTimeMetric(String name, long totalTime, long exclusiveTime, TimeUnit timeUnit);

    /**
     * Record a metric value for the given name.
     * 
     * @param name The name of the metric. The metric is not recorded if the name is null or the empty string.
     * @param value The value of the metric.
     */
    void recordMetric(String name, float value);

    /**
     * Record a response time in milliseconds for the given metric name.
     * 
     * @param name The name of the metric. The response time is not recorded if the name is null or the empty string.
     * @param millis The response time in milliseconds.
     */
    void recordResponseTimeMetric(String name, long millis);

    /**
     * Increment the metric counter for the given name.
     * 
     * @param name The name of the metric to increment.
     */
    void incrementCounter(String name);

    /**
     * Increment the metric counter for the given name.
     * 
     * @param name The name of the metric to increment.
     * @param count The amount in which the metric should be incremented.
     */
    void incrementCounter(String name, int count);
}
