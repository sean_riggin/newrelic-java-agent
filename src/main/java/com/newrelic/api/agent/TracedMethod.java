package com.newrelic.api.agent;

/**
 * Represents a single instance of the timing mechanism associated with a method that is instrumented using the
 * {@link Trace} annotation.
 * 
 * @author sdaubin
 * @see Agent#getTracedMethod()
 * 
 */
public interface TracedMethod {

    /**
     * Returns the traced method metric name.
     * 
     */
    String getMetricName();

    /**
     * Sets the traced method metric name.
     * 
     * @param metricNameParts The segments of the metric name. These values will be concatenated together separated by a
     *        `/` char.
     */
    void setMetricName(String... metricNameParts);

    /**
     * Metric names added here will be reported as roll-up metrics. A rollup metric is an extra unscoped metric that is
     * reported in addition to the normal metric recorded for a traced method. An example of how the agent uses a rollup
     * metric is the Database/all metric. Database tracers record a metric for specific table operations,
     * Database/users/select for example, but they also record a Database/all rollup metric which represents all
     * database operations.
     * 
     * @param metricNameParts The segments of the rollup metric name. These values will be concatenated together
     *        separated by a `/` char.
     */
    void addRollupMetricName(String... metricNameParts);

}
